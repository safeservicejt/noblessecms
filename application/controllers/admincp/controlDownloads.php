<?php

class controlDownloads
{
	public function index()
	{
		
		$post=array('alert'=>'');

		Model::load('admincp/downloads');
		
		$curPage=0;

		if($match=Uri::match('\/page\/(\d+)'))
		{
			$curPage=$match[1];
		}

		if(Request::has('btnAction'))
		{
			actionProcess();
		}

		if(Request::has('btnAdd'))
		{
			try {
				
				insertProcess();

				$post['alert']='<div class="alert alert-success">Add new file success.</div>';

			} catch (Exception $e) {
				$post['alert']='<div class="alert alert-warning">'.$e->getMessage().'</div>';
			}
		}

		if(Request::has('btnSave'))
		{
			$match=Uri::match('\/edit\/(\d+)');

			try {
				
				updateProcess($match[1]);

				$post['alert']='<div class="alert alert-success">Update file success.</div>';

			} catch (Exception $e) {
				$post['alert']='<div class="alert alert-warning">'.$e->getMessage().'</div>';
			}
		}

		if(Request::has('btnSearch'))
		{
			filterProcess();
		}
		else
		{
			$post['pages']=Misc::genSmallPage('admincp/downloads',$curPage);

			$post['theList']=Downloads::get(array(
				'limitShow'=>20,
				'limitPage'=>$curPage,
				'orderby'=>'order by downloadid desc',
				'cache'=>'no'
				));
		}

		if($match=Uri::match('\/edit\/(\d+)'))
		{
			$loadData=Downloads::get(array(
				'where'=>"where downloadid='".$match[1]."'",
				'cache'=>'no'
				));

			$post['edit']=$loadData[0];
		}
		
		System::setTitle('File list - '.ADMINCP_TITLE);

		View::make('admincp/head');

		self::makeContents('downloadList',$post);

		View::make('admincp/footer');

	}

    public function makeContents($viewPath,$inputData=array())
    {
        View::make('admincp/left');  

        View::make('admincp/'.$viewPath,$inputData);
    }
}

?>